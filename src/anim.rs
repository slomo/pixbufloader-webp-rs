use std::{
    borrow::Borrow,
    fs::File,
    io::{BufReader, Read},
    mem::zeroed,
    ops::Sub,
    os::fd::{FromRawFd, RawFd},
    ptr::null_mut,
    rc::Rc,
    time::Duration,
};

use gdk_pixbuf::{
    subclass::prelude::ObjectSubclassIsExt, Pixbuf, PixbufAnimation, PixbufAnimationIter,
};
use glib::{
    ffi::{g_clear_error, GError},
    translate::ToGlibPtr,
};

use libc::c_int;
use libwebp_sys::{
    WebPAnimDecoderGetInfo, WebPAnimDecoderGetNext, WebPAnimDecoderHasMoreFrames,
    WebPAnimDecoderOptions, WebPAnimDecoderOptionsInit, WebPBitstreamFeatures, WebPGetFeatures,
    VP8_STATUS_OK,
};

use crate::wrappers::AnimDecWrapper;

mod imp {
    use std::cell::RefCell;
    use std::ops::Sub;
    use std::rc::Rc;
    use std::time::{Duration, SystemTime};

    use gdk_pixbuf::{
        subclass::prelude::{PixbufAnimationImpl, PixbufAnimationIterImpl},
        Pixbuf, PixbufAnimationIter,
    };

    use glib::{subclass::prelude::*, Cast};

    #[derive(Default)]
    pub struct WebPAnimation {
        pub anim_data: RefCell<Rc<Vec<u8>>>,
        pub frame_size: RefCell<(i32, i32)>,
        pub is_static: RefCell<bool>,
        cached_static_image: RefCell<Option<Pixbuf>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for WebPAnimation {
        // This type name must be unique per process.
        const NAME: &'static str = "WebPAnimation";

        type Type = super::WebPAnimation;

        // The parent type this one is inheriting from.
        // Optional, if not specified it defaults to `glib::Object`
        type ParentType = gdk_pixbuf::PixbufAnimation;

        // Interfaces this type implements.
        // Optional, if not specified it defaults to `()`
        type Interfaces = ();
    }

    impl ObjectImpl for WebPAnimation {}

    impl PixbufAnimationImpl for WebPAnimation {
        fn is_static_image(&self) -> bool {
            *self.is_static.borrow()
        }

        fn static_image(&self) -> Option<Pixbuf> {
            if self.cached_static_image.borrow().is_none() {
                let iter = self.iter(
                    SystemTime::now()
                        .duration_since(SystemTime::UNIX_EPOCH)
                        .unwrap(),
                );
                self.cached_static_image.replace(Some(iter.pixbuf()));
            }

            self.cached_static_image.borrow().clone()
        }

        fn size(&self) -> (i32, i32) {
            *self.frame_size.borrow()
        }

        fn iter(&self, start_time: Duration) -> PixbufAnimationIter {
            let iter = super::WebPAnimationIter::new_from_buffer_and_time(
                self.anim_data.borrow().clone(),
                start_time,
            )
            .unwrap();
            iter.upcast()
        }
    }

    #[derive(Default)]
    pub struct WebPAnimationIter {
        pub start_time: RefCell<Duration>,
        pub curr_time: RefCell<Duration>,
        pub frames: RefCell<Vec<(Pixbuf, Duration)>>,
        pub loop_length: RefCell<Duration>,
        curr_frame: RefCell<usize>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for WebPAnimationIter {
        // This type name must be unique per process.
        const NAME: &'static str = "WebPAnimationIter";

        type Type = super::WebPAnimationIter;

        // The parent type this one is inheriting from.
        // Optional, if not specified it defaults to `glib::Object`
        type ParentType = gdk_pixbuf::PixbufAnimationIter;

        // Interfaces this type implements.
        // Optional, if not specified it defaults to `()`
        type Interfaces = ();

        fn new() -> Self {
            let now = SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .unwrap();
            Self {
                frames: RefCell::new(Vec::new()),
                start_time: RefCell::new(now),
                curr_time: RefCell::new(now),
                loop_length: RefCell::new(Duration::default()),
                curr_frame: RefCell::new(0),
            }
        }
    }

    impl ObjectImpl for WebPAnimationIter {}

    impl PixbufAnimationIterImpl for WebPAnimationIter {
        fn delay_time(&self) -> Option<Duration> {
            match self.frames.borrow().len() {
                0 => None,
                _ => Some(self.frames.borrow()[*self.curr_frame.borrow()].1),
            }
        }

        fn pixbuf(&self) -> Pixbuf {
            let frames = self.frames.borrow();

            if frames.len() == 0 {
                panic!("No PixbufAnimation frames available");
            };

            frames[*self.curr_frame.borrow()].0.clone()
        }

        fn on_currently_loading_frame(&self) -> bool {
            false
        }

        fn advance(&self, time: Duration) -> bool {
            if &time <= &self.curr_time.borrow() {
                return false;
            }

            let delta = time.sub(*self.start_time.borrow());
            self.curr_time.replace(time);

            let mut remainder_milli = delta.as_millis() % self.loop_length.borrow().as_millis();

            let mut frame = 0;
            for i in self.frames.borrow().iter().map(|x| &x.1) {
                let frame_len = i.as_millis();

                if remainder_milli <= frame_len {
                    break;
                }

                remainder_milli -= frame_len;
                frame += 1;
            }

            if *self.curr_frame.borrow() != frame {
                self.curr_frame.replace(frame);
                true
            } else {
                false
            }
        }
    }
}

glib::wrapper! {
    pub struct WebPAnimation(ObjectSubclass<imp::WebPAnimation>) @extends PixbufAnimation;
}

glib::wrapper! {
    pub struct WebPAnimationIter(ObjectSubclass<imp::WebPAnimationIter>) @extends PixbufAnimationIter;
}

impl WebPAnimation {
    pub fn new_from_fd(fd: RawFd, error: *mut *mut GError) -> Option<Self> {
        unsafe { g_clear_error(error) };

        let mut data: Vec<u8> = Vec::new();

        let file = unsafe { File::from_raw_fd(fd) };

        let mut reader = BufReader::new(file);
        match reader.read_to_end(&mut data) {
            Err(e) => {
                if error != null_mut() {
                    let msg = format!(
                        "Could not read WebP file contents for WebP file {:?}.",
                        e.to_string()
                    );
                    unsafe {
                        *error = glib::Error::new(gdk_pixbuf::PixbufError::CorruptImage, &msg)
                            .to_glib_full() as *mut GError
                    };
                }
                return None;
            }
            Ok(_) => {}
        }

        Self::new_from_buffer(data, error)
    }

    pub fn new_from_buffer(data: Vec<u8>, error: *mut *mut GError) -> Option<Self> {
        unsafe { g_clear_error(error) };

        let mut features: WebPBitstreamFeatures = unsafe { zeroed() };

        let ret = unsafe {
            WebPGetFeatures(
                data.as_ptr(),
                data.len(),
                &mut features as *mut WebPBitstreamFeatures,
            )
        };

        if ret != VP8_STATUS_OK {
            if error != null_mut() {
                let msg = format!("Could not get WebP informtation, WebPBitstreamFeatures exited with error code {}.", ret);
                unsafe {
                    *error = glib::Error::new(gdk_pixbuf::PixbufError::CorruptImage, &msg)
                        .to_glib_full() as *mut GError
                };
            }
            return None;
        }

        let slf: WebPAnimation = glib::Object::new(&[]);
        let anim_imp = slf.imp();

        anim_imp.is_static.replace(features.has_animation == 0);
        anim_imp
            .frame_size
            .replace((features.width, features.height));

        anim_imp.anim_data.replace(Rc::new(data));

        Some(slf)
    }
}

impl WebPAnimationIter {
    pub fn new_from_buffer_and_time(
        buffer: Rc<Vec<u8>>,
        start_time: Duration,
    ) -> Result<Self, &'static str> {
        let slf: WebPAnimationIter = glib::Object::new(&[]);
        {
            let iter_imp = slf.imp();
            iter_imp.curr_time.replace(start_time);
            iter_imp.start_time.replace(start_time);

            let data: &Vec<u8> = buffer.borrow();

            let mut opts: WebPAnimDecoderOptions = unsafe { zeroed() };
            if unsafe { WebPAnimDecoderOptionsInit(&mut opts) } == 0 {
                return Err("Could not initialize WebPAnimDecoderOptions");
            }

            opts.color_mode = libwebp_sys::MODE_RGBA;

            let decoder = unsafe { AnimDecWrapper::new(&data, &opts) };
            if decoder.0 == null_mut() {
                return Err("Could not instantiate WebPAnimDecoder");
            }

            let mut animinfo = unsafe { zeroed() };
            if unsafe { WebPAnimDecoderGetInfo(decoder.0, &mut animinfo) } == 0 {
                return Err("Could not get WebP animation info for data");
            }

            let mut tmp_data = null_mut();
            let mut timestamp: c_int = 0;

            while unsafe { WebPAnimDecoderHasMoreFrames(decoder.0) } != 0 {
                if unsafe { WebPAnimDecoderGetNext(decoder.0, &mut tmp_data, &mut timestamp) } == 0
                {
                    return Err("Could not get first frame");
                }

                let pb = Pixbuf::new(
                    gdk_pixbuf::Colorspace::Rgb,
                    true,
                    8,
                    animinfo.canvas_width as i32,
                    animinfo.canvas_height as i32,
                );

                let pb = match pb {
                    Some(pb) => pb,
                    None => {
                        return Err("Could not allocate new Pixbuf");
                    }
                };

                let pb_pixels = unsafe { pb.pixels() }.as_mut_ptr();
                let pb_stride = pb.rowstride() as usize;
                let data_stride = animinfo.canvas_width as usize * 4;
                for row in 0..(animinfo.canvas_height as usize) {
                    let data_row = unsafe { tmp_data.offset(row as isize * data_stride as isize) };
                    let pb_row = unsafe { pb_pixels.offset(row as isize * pb_stride as isize) };
                    unsafe { std::ptr::copy(data_row, pb_row, data_stride) };
                }

                {
                    let mut frames = iter_imp.frames.borrow_mut();
                    let loop_length = iter_imp.loop_length.borrow();
                    let frame_duration = Duration::from_millis(timestamp as u64).sub(*loop_length);
                    frames.push((pb, frame_duration.clone()));
                }

                iter_imp
                    .loop_length
                    .replace(Duration::from_millis(timestamp as u64));

                tmp_data = null_mut();
                timestamp = 0;
            }
        }

        Ok(slf)
    }
}

#[cfg(test)]
mod tests {
    use gdk_pixbuf::subclass::prelude::{ObjectSubclassIsExt, PixbufAnimationImpl};
    use glib::{ffi::GError, translate::FromGlibPtrFull};
    use std::{
        fs::File,
        os::fd::IntoRawFd,
        path::PathBuf,
        ptr::null_mut,
        time::{Duration, SystemTime},
    };

    use super::WebPAnimation;

    #[test]
    fn anim() {
        let mut res = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        res.push("testdata/1px_frame_anim.webp");
        let webp_file = File::open(res).unwrap();

        let mut error: *mut GError = null_mut();

        let animation = WebPAnimation::new_from_fd(webp_file.into_raw_fd(), &mut error);

        if error != null_mut() {
            let error = unsafe { glib::Error::from_glib_full(error) };
            panic!("GError: {}", error.message());
        }

        let st_now = SystemTime::now();
        let now = st_now.duration_since(SystemTime::UNIX_EPOCH).unwrap();
        let next1 = st_now.checked_add(Duration::from_millis(1100)).unwrap();
        let next2 = next1.checked_add(Duration::from_millis(1000)).unwrap();
        let next3 = next2.checked_add(Duration::from_millis(1000)).unwrap();
        let next4 = next3.checked_add(Duration::from_millis(1000)).unwrap();

        let iter = animation.unwrap().imp().iter(now.clone());

        let pb = iter.pixbuf();
        assert_eq!(unsafe { pb.pixels() }, &[0xb6, 0x6a, 0x1d, 0x84]);

        iter.advance(next1);
        let pb = iter.pixbuf();
        assert_eq!(unsafe { pb.pixels() }, &[0xe4, 0x19, 0x28, 0xd0]);

        iter.advance(next2);
        let pb = iter.pixbuf();
        assert_eq!(unsafe { pb.pixels() }, &[0x4d, 0x0f, 0xaf, 0xed]);

        iter.advance(next3);
        let pb = iter.pixbuf();
        assert_eq!(unsafe { pb.pixels() }, &[0x1c, 0x74, 0x38, 0xf9]);

        iter.advance(next4);
        let pb = iter.pixbuf();
        assert_eq!(unsafe { pb.pixels() }, &[0xbc, 0xc3, 0x11, 0xfd]);
    }
}
