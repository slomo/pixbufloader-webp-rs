mod anim;
mod wrappers;

use std::ptr::{null, null_mut};
use std::slice::from_raw_parts;

use anim::WebPAnimation;
use gdk_pixbuf::ffi::{
    gdk_pixbuf_get_has_alpha, gdk_pixbuf_get_height, gdk_pixbuf_get_pixels_with_length,
    gdk_pixbuf_get_rowstride, gdk_pixbuf_get_width, GdkPixbuf, GdkPixbufAnimation, GdkPixbufFormat,
    GdkPixbufModule, GdkPixbufModulePattern, GdkPixbufModulePreparedFunc, GdkPixbufModuleSizeFunc,
    GdkPixbufModuleUpdatedFunc, GDK_PIXBUF_FORMAT_SCALABLE, GDK_PIXBUF_FORMAT_THREADSAFE,
};
use gdk_pixbuf::prelude::PixbufAnimationExtManual;
use gdk_pixbuf::{Pixbuf, PixbufAnimation};

use glib::Cast;
use libc::{c_char, c_int, c_uint, FILE};

use glib::ffi::{gboolean, gpointer, GError};
use glib::translate::{FromGlibPtrFull, IntoGlib, IntoGlibPtr, ToGlibPtr};

use cstr::cstr;

use libwebp_sys::{
    WebPBitstreamFeatures, WebPDecoderConfig, WebPGetFeatures, WebPGetInfo, WebPIAppend,
    WebPIDecGetRGB, WebPIDecode, WebPIDecoder, WebPIDelete, VP8_STATUS_OK, VP8_STATUS_SUSPENDED,
};

struct LoaderContext {
    size_func: GdkPixbufModuleSizeFunc,
    prep_func: GdkPixbufModulePreparedFunc,
    update_func: GdkPixbufModuleUpdatedFunc,
    user_data: gpointer,
    dec_config: WebPDecoderConfig,
    got_header: bool,
    pixbuf: *mut GdkPixbuf,
    idec: *mut WebPIDecoder,
    anim_buffer: Option<Vec<u8>>,
}

impl Drop for LoaderContext {
    fn drop(&mut self) {
        if self.idec != null_mut() {
            unsafe { WebPIDelete(self.idec) };
        }
        if self.pixbuf != null_mut() {
            let _ = unsafe { Pixbuf::from_glib_full(self.pixbuf) };
        }
    }
}

#[no_mangle]
unsafe extern "C" fn begin_load(
    size_func: GdkPixbufModuleSizeFunc,
    prep_func: GdkPixbufModulePreparedFunc,
    update_func: GdkPixbufModuleUpdatedFunc,
    user_data: gpointer,
    error: *mut *mut GError,
) -> gpointer {
    if !error.is_null() {
        *error = null_mut();
    }

    let ctx = Box::new(LoaderContext {
        size_func,
        prep_func,
        update_func,
        user_data,
        dec_config: unsafe { std::mem::zeroed() },
        got_header: false,
        pixbuf: null_mut(),
        idec: null_mut(),
        anim_buffer: None,
    });

    Box::into_raw(ctx) as gpointer
}

#[no_mangle]
unsafe extern "C" fn load_increment(
    user_data: gpointer,
    buffer: *const u8,
    size: c_uint,
    error: *mut *mut GError,
) -> gboolean {
    if !error.is_null() {
        *error = null_mut();
    }

    let ctx = user_data as *mut LoaderContext;
    let ctx = &mut *ctx;

    if !ctx.got_header {
        let mut width: c_int = 0;
        let mut height: c_int = 0;
        let rc = WebPGetInfo(
            buffer,
            size as usize,
            &mut width as *mut c_int,
            &mut height as *mut c_int,
        );
        if rc == 0 {
            if error != null_mut() {
                *error = glib::Error::new(
                    gdk_pixbuf::PixbufError::Failed,
                    "Could not get WebP header information",
                )
                .to_glib_full() as *mut GError;
            }
            return false.into_glib();
        }

        ctx.got_header = true;

        if let Some(size_func) = ctx.size_func {
            let mut scaled_w = width;
            let mut scaled_h = height;

            size_func(&mut scaled_w, &mut scaled_h, ctx.user_data);
            if scaled_w != 0 && scaled_h != 0 && (scaled_w != width || scaled_h != height) {
                ctx.dec_config.options.use_scaling = true.into_glib();
                ctx.dec_config.options.scaled_width = scaled_w;
                ctx.dec_config.options.scaled_height = scaled_h;
                width = scaled_w;
                height = scaled_h;
            };
        }

        let mut features: WebPBitstreamFeatures = unsafe { std::mem::zeroed() };
        if WebPGetFeatures(buffer, size as usize, &mut features) != VP8_STATUS_OK {
            if error != null_mut() {
                *error = glib::Error::new(
                    gdk_pixbuf::PixbufError::Failed,
                    "Could not get WebP image features",
                )
                .to_glib_full() as *mut GError;
            }
            return false.into_glib();
        };

        if features.has_animation != 0 {
            ctx.dec_config.input.has_animation = true.into_glib();

            let buf_slice = unsafe { from_raw_parts(buffer, size as usize) };

            let mut anim_buffer: Vec<u8> = Vec::new();
            anim_buffer.extend_from_slice(buf_slice);

            ctx.anim_buffer = Some(anim_buffer);

            return true.into_glib();
        }

        ctx.pixbuf = gdk_pixbuf::ffi::gdk_pixbuf_new(
            gdk_pixbuf::ffi::GDK_COLORSPACE_RGB,
            features.has_alpha,
            8,
            width,
            height,
        );

        if ctx.pixbuf == null_mut() {
            if error != null_mut() {
                *error = glib::Error::new(
                    gdk_pixbuf::PixbufError::Failed,
                    "Could not create pixbuf instance",
                )
                .to_glib_full() as *mut GError;
            }
            return false.into_glib();
        };

        ctx.dec_config.output.colorspace = match gdk_pixbuf_get_has_alpha(ctx.pixbuf) != 0 {
            true => libwebp_sys::MODE_RGBA,
            false => libwebp_sys::MODE_RGB,
        };
        let mut length: c_uint = 0;
        ctx.dec_config.output.is_external_memory = true.into_glib();
        ctx.dec_config.output.u.RGBA.rgba =
            gdk_pixbuf_get_pixels_with_length(ctx.pixbuf, &mut length as *mut c_uint);
        ctx.dec_config.output.u.RGBA.stride = gdk_pixbuf_get_rowstride(ctx.pixbuf);
        ctx.dec_config.output.u.RGBA.size = length as usize;

        ctx.idec = WebPIDecode(null(), 0, &mut ctx.dec_config as *mut WebPDecoderConfig);

        if let Some(prep_func) = ctx.prep_func.as_ref() {
            prep_func(ctx.pixbuf, null_mut(), ctx.user_data);
        }
    };

    /* Append size bytes to decoder's buffer */
    let status = WebPIAppend(ctx.idec, buffer, size as usize);
    if status != VP8_STATUS_SUSPENDED && status != VP8_STATUS_OK {
        if error != null_mut() {
            let msg = format!("WebP decoder failed with status code {:?}.", status);
            *error = glib::Error::new(gdk_pixbuf::PixbufError::CorruptImage, &msg).to_glib_full()
                as *mut GError;
        }
        return false.into_glib();
    }

    /* Decode decoder's updated buffer */
    let mut last_y = 0;
    let mut width = 0;
    if WebPIDecGetRGB(ctx.idec, &mut last_y, &mut width, null_mut(), null_mut()) == null_mut()
        && status != VP8_STATUS_SUSPENDED
    {
        if error != null_mut() {
            let msg = format!("Bad inputs to WebP decoder, status {:?}.", status);
            *error = glib::Error::new(gdk_pixbuf::PixbufError::Failed, &msg).to_glib_full()
                as *mut GError;
        }
        return false.into_glib();
    }

    if let Some(update_func) = ctx.update_func.as_ref() {
        update_func(ctx.pixbuf, 0, 0, width, last_y, ctx.user_data);
    }

    true.into_glib()
}

#[no_mangle]
unsafe extern "C" fn stop_load(user_data: gpointer, error: *mut *mut GError) -> gboolean {
    if !error.is_null() {
        *error = null_mut();
    }

    // We own the object again so that it is dropped when stop_load ends
    let mut ctx = Box::from_raw(user_data as *mut LoaderContext);

    if ctx.anim_buffer.is_some() {
        let buf = ctx.anim_buffer.take().unwrap();
        let anim = WebPAnimation::new_from_buffer(buf, error);

        let anim = match anim {
            Some(anim) => anim,
            None => {
                return false.into_glib();
            }
        };

        let pbanim: PixbufAnimation = anim.upcast();

        let iter = pbanim.iter(None);
        let pb = iter.pixbuf().copy().unwrap();

        ctx.pixbuf = pb.into_glib_ptr();
        let h = gdk_pixbuf_get_height(ctx.pixbuf);
        let w = gdk_pixbuf_get_width(ctx.pixbuf);

        if let Some(prep_func) = ctx.prep_func {
            prep_func(ctx.pixbuf, pbanim.to_glib_none().0, ctx.user_data);
        }

        // The module loader increases a ref so we drop the pixbuf reference on LoaderContext::drop
        if let Some(update_func) = ctx.update_func {
            update_func(ctx.pixbuf, 0, 0, w, h, ctx.user_data);
        }
    }

    if !ctx.pixbuf.is_null() {
        let h = gdk_pixbuf_get_height(ctx.pixbuf);
        let w = gdk_pixbuf_get_width(ctx.pixbuf);

        // The module loader increases a ref so we drop the pixbuf reference on LoaderContext::drop
        if let Some(update_func) = ctx.update_func {
            update_func(ctx.pixbuf, 0, 0, w, h, ctx.user_data);
        }
    }

    true.into_glib()
}

#[no_mangle]
unsafe extern "C" fn load_animation(
    animation_file: *mut FILE,
    error: *mut *mut GError,
) -> *mut GdkPixbufAnimation {
    let fd = unsafe { libc::fileno(animation_file) };

    if fd == -1 {
        if error != null_mut() {
            unsafe {
                *error = glib::Error::new(
                    gdk_pixbuf::PixbufError::CorruptImage,
                    &"Could not create file descriptor from *FILE stream with WebP data.",
                )
                .to_glib_full() as *mut GError
            };
            return null_mut();
        }
    }

    match crate::anim::WebPAnimation::new_from_fd(fd, error) {
        Some(animation) => {
            let animation: PixbufAnimation = animation.upcast();
            animation.into_glib_ptr()
        }
        None => null_mut(),
    }
}

#[no_mangle]
extern "C" fn fill_vtable(module: &mut GdkPixbufModule) {
    module.begin_load = Some(begin_load);
    module.stop_load = Some(stop_load);
    module.load_increment = Some(load_increment);
    module.load_animation = Some(load_animation);
}

const SIGNATURE: [GdkPixbufModulePattern; 2] = [
    GdkPixbufModulePattern {
        prefix: cstr!("RIFFsizeWEBP").as_ptr() as *mut c_char,
        mask: cstr!("    xxxx    ").as_ptr() as *mut c_char,
        relevance: 100,
    },
    GdkPixbufModulePattern {
        prefix: std::ptr::null_mut(),
        mask: std::ptr::null_mut(),
        relevance: 0,
    },
];

const MIME_TYPES: [*const c_char; 3] = [
    cstr!("image/webp").as_ptr(),
    cstr!("audio/x-riff").as_ptr(),
    std::ptr::null(),
];

const EXTENSIONS: [*const c_char; 2] = [cstr!("webp").as_ptr(), std::ptr::null()];

#[no_mangle]
extern "C" fn fill_info(info: &mut GdkPixbufFormat) {
    info.name = cstr!("webp").as_ptr() as *mut c_char;
    info.signature = SIGNATURE.as_ptr() as *mut GdkPixbufModulePattern;
    info.description = cstr!("The WebP image format").as_ptr() as *mut c_char; //TODO: Gettext this
    info.mime_types = MIME_TYPES.as_ptr() as *mut *mut c_char;
    info.extensions = EXTENSIONS.as_ptr() as *mut *mut c_char;
    info.flags = GDK_PIXBUF_FORMAT_SCALABLE | GDK_PIXBUF_FORMAT_THREADSAFE;
    info.license = cstr!("LGPL").as_ptr() as *mut c_char;
}

#[cfg(test)]
mod tests {
    use crate::tests::base64::Engine;

    use gdk_pixbuf::ffi::{
        gdk_pixbuf_get_pixels, GdkPixbuf, GdkPixbufFormat, GDK_PIXBUF_FORMAT_SCALABLE,
        GDK_PIXBUF_FORMAT_THREADSAFE,
    };
    use glib::{ffi::GError, translate::IntoGlib};

    use crate::{EXTENSIONS, MIME_TYPES};
    use libc::{c_char, c_int, c_void};
    use std::ptr::{null, null_mut};

    extern crate base64;

    fn pb_format_new() -> GdkPixbufFormat {
        let mut info = super::GdkPixbufFormat {
            name: null_mut(),
            signature: null_mut(),
            description: null_mut(),
            mime_types: null_mut(),
            extensions: null_mut(),
            flags: 0,
            license: null_mut(),
            disabled: false.into_glib(),
            domain: null_mut(),
        };

        super::fill_info(&mut info);
        info
    }

    #[test]
    fn fill_info() {
        let info = pb_format_new();

        assert_ne!(info.name, null_mut());
        assert_ne!(info.signature, null_mut());
        assert_ne!(info.description, null_mut());
        assert_ne!(info.mime_types, null_mut());
        assert_ne!(info.extensions, null_mut());
        assert_eq!(
            info.flags,
            GDK_PIXBUF_FORMAT_SCALABLE | GDK_PIXBUF_FORMAT_THREADSAFE
        );
        assert_ne!(info.license, null_mut());
    }

    fn check_null_terminated_arr_cstrings(arr: &[*const c_char]) {
        let n_strings = arr
            .iter()
            .filter(|e| e != &&null())
            .map(|e| {
                if e != &null() {
                    // We use strlen in all of them to ensure some safety
                    // We could use CStr instead but it'd be a bit more cumbersome
                    assert!(unsafe { libc::strlen(*e as *const c_char) } > 0)
                }
            })
            .count(); // Count all non_null items

        // Ensure last item is null and is the only null item
        assert_eq!(n_strings, arr.len() - 1);
        assert!(arr.last().unwrap() == &null());
    }

    #[test]
    fn extensions_bounds() {
        check_null_terminated_arr_cstrings(&EXTENSIONS);
    }

    #[test]
    fn mime_bounds() {
        check_null_terminated_arr_cstrings(&MIME_TYPES)
    }

    #[test]
    fn signature() {
        let info = pb_format_new();

        for i in 0..1 {
            let ptr = unsafe { info.signature.offset(i) };
            if i == 1 {
                assert_eq!(unsafe { (*ptr).prefix }, null_mut());
                continue;
            } else {
                assert_ne!(unsafe { (*ptr).prefix }, null_mut());
                if unsafe { (*ptr).mask } != null_mut() {
                    // Mask can be null
                    assert_eq!(
                        unsafe { libc::strlen((*ptr).prefix as *mut c_char) },
                        unsafe { libc::strlen((*ptr).mask as *mut c_char) }
                    );
                }
                // Relevance must be 0 to 100
                assert!(unsafe { (*ptr).relevance } >= 0);
                assert!(unsafe { (*ptr).relevance } <= 100);
            }
        }
    }

    const TEST1_WEBP_DATA: &'static str = "UklGRgIDAABXRUJQVlA4WAoAAAAwAAAAAAAAAAAASUNDUKACAAAAAAKgbGNtcwQwAABtbnRyUkdCIFhZWiAH5AABAAkAFwArABlhY3NwQVBQTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA9tYAAQAAAADTLWxjbXMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA1kZXNjAAABIAAAAEBjcHJ0AAABYAAAADZ3dHB0AAABmAAAABRjaGFkAAABrAAAACxyWFlaAAAB2AAAABRiWFlaAAAB7AAAABRnWFlaAAACAAAAABRyVFJDAAACFAAAACBnVFJDAAACFAAAACBiVFJDAAACFAAAACBjaHJtAAACNAAAACRkbW5kAAACWAAAACRkbWRkAAACfAAAACRtbHVjAAAAAAAAAAEAAAAMZW5VUwAAACQAAAAcAEcASQBNAFAAIABiAHUAaQBsAHQALQBpAG4AIABzAFIARwBCbWx1YwAAAAAAAAABAAAADGVuVVMAAAAaAAAAHABQAHUAYgBsAGkAYwAgAEQAbwBtAGEAaQBuAABYWVogAAAAAAAA9tYAAQAAAADTLXNmMzIAAAAAAAEMQgAABd7///MlAAAHkwAA/ZD///uh///9ogAAA9wAAMBuWFlaIAAAAAAAAG+gAAA49QAAA5BYWVogAAAAAAAAJJ8AAA+EAAC2xFhZWiAAAAAAAABilwAAt4cAABjZcGFyYQAAAAAAAwAAAAJmZgAA8qcAAA1ZAAAT0AAACltjaHJtAAAAAAADAAAAAKPXAABUfAAATM0AAJmaAAAmZwAAD1xtbHVjAAAAAAAAAAEAAAAMZW5VUwAAAAgAAAAcAEcASQBNAFBtbHVjAAAAAAAAAAEAAAAMZW5VUwAAAAgAAAAcAHMAUgBHAEJBTFBIAgAAAABwVlA4IDIAAADQAQCdASoBAAEAAMASJaACdLoB+AADsAD+54v//DlPhynw5T/hyn/vGf7jP9xn/yIAAA==";

    #[test]
    fn test_basic_webp() {
        unsafe extern "C" fn prep_cb(
            pb: *mut gdk_pixbuf::ffi::GdkPixbuf,
            pba: *mut gdk_pixbuf::ffi::GdkPixbufAnimation,
            user_data: *mut libc::c_void,
        ) {
            assert_eq!(user_data, null_mut());
            assert_eq!(pba, null_mut());

            let w = gdk_pixbuf::ffi::gdk_pixbuf_get_width(pb);
            let h = gdk_pixbuf::ffi::gdk_pixbuf_get_height(pb);

            assert_eq!(w, 1);
            assert_eq!(h, 1);
        }

        unsafe extern "C" fn upd_cb(
            pb: *mut GdkPixbuf,
            _x: c_int,
            _y: c_int,
            _w: c_int,
            _h: c_int,
            _data: *mut c_void,
        ) {
            let pixels = gdk_pixbuf_get_pixels(pb);

            // This is a 1x1 image with decimal values of A: 182, R: 106, G: 29, B: 112
            assert_eq!(*pixels, 182);
            assert_eq!(*pixels.offset(1), 106);
            assert_eq!(*pixels.offset(2), 29);
            assert_eq!(*pixels.offset(3), 112);
        }

        let bytes = base64::engine::general_purpose::STANDARD
            .decode(TEST1_WEBP_DATA)
            .unwrap();

        let ctx =
            unsafe { crate::begin_load(None, Some(prep_cb), Some(upd_cb), null_mut(), null_mut()) };
        assert_ne!(ctx, null_mut());

        let mut error = null_mut() as *mut GError;
        unsafe {
            let _ = crate::load_increment(
                ctx,
                bytes.as_ptr(),
                bytes.len() as u32,
                &mut error as *mut *mut GError,
            );
        };

        assert_eq!(error, null_mut(), "{:?}", unsafe {
            &std::ffi::CString::from_raw((*error).message)
        });

        assert_eq!(
            unsafe { crate::stop_load(ctx as *mut c_void, null_mut()) },
            true.into_glib()
        );
    }
}
