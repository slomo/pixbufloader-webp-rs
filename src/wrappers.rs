use std::ptr::null_mut;

use libwebp_sys::{
    WebPAnimDecoder, WebPAnimDecoderDelete, WebPAnimDecoderNew, WebPAnimDecoderOptions, WebPData,
};

pub struct AnimDecWrapper(pub *mut WebPAnimDecoder);

impl Default for AnimDecWrapper {
    fn default() -> Self {
        Self(null_mut())
    }
}

impl AnimDecWrapper {
    pub unsafe fn new(data: &[u8], opts: &WebPAnimDecoderOptions) -> Self {
        let data = WebPData {
            bytes: data.as_ptr(),
            size: data.len(),
        };
        Self(WebPAnimDecoderNew(&data, opts))
    }
}

impl Drop for AnimDecWrapper {
    fn drop(&mut self) {
        if self.0 != null_mut() {
            unsafe { WebPAnimDecoderDelete(self.0) };
        }
    }
}
